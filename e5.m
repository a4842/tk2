day = [184; 230; 276; 322; 368; 414; 460; 506];
covid = [3075; 4105; 8369; 11287; 6971; 6279; 6594; 38325];
n = length(day);

first = 184;
last = 506;
xnew = linspace(first, last, last-first+1);

#Condition 1
con1 = zeros(7, 1);
for i=1:n-1
  con1(i, 1) = covid(i);
endfor

#Condtion 2
printf("condtion %d\n", 2);
for i=1:n-1
  h = day(i+1) - day(i);
  printf("i = %d\n", i);
  printf("a%d + b%d + c%d + d = %d\n", h^3, h^2, h, covid(i+1));
endfor

#Condtion 3
printf("condtion %d\n", 3);
for i=1:n-2
  h = day(i+1) - day(i);
  printf("i = %d\n", i);
  printf("a%d + b%d + c = c_(i+1)\n", 3*h^2, 2*h);
endfor

#Condtion 4
printf("condtion %d\n", 4);
for i=1:n-2
  h = day(i+1) - day(i);
  printf("i = %d\n", i);
  printf("a%d + 2b = 2b_(i+1)\n", 6*h);
endfor

abcMatrix = [
 -44721 13813 -31687 3075; 
 -44721 76418 67006 4105; 
 -24693 14703 10892 8369;
 42753 -32606 -34303 11287;
 -34769 26394 -62882 6971;
 69436 -21588 -40776 6279;
 69436 74234 20140 6594
]

[y_new] = calculateSpline(xnew, abcMatrix);

plot(xnew, y_new);