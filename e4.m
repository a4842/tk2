day = [184; 230; 276; 322; 368; 414; 460; 506];
covid = [3075; 4105; 8369; 11287; 6971; 6279; 6594; 38325];

day_extra = [day; 552];
covid_extra = [covid; 5403];

first = 184;
last = 506;
xnew = linspace(first, last, last-first+1);

[coef1, dd1] = newtonInterpolCoef(day, covid);
[coef2, dd2] = newtonInterpolCoefExtra(coef1, dd1, day_extra, covid_extra);
[ynew1] = newtonInterpolCalc(coef1, day, xnew);
[ynew2] = newtonInterpolCalc(coef2, day_extra, xnew);

% Plot polynomial interpolation degree 7
figure
plot(xnew, ynew1);

% Plot polynomial interpolation degree 8
figure
plot(xnew, ynew2);