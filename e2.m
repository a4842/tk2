day = [184; 230; 276; 322; 368; 414; 460; 506];
covid = [3075; 4105; 8369; 11287; 6971; 6279; 6594; 38325];

base = @(t, i) ((t - 460) / 30) ^ i;
A = genInterpolMtx(base, day, covid);
coef = A \ covid;

x = linspace(day(1), day(8), day(8)-day(1)+1);
y = hornerInterpol(coef, base, x)

# Printing interpolation formula in horner form
myfunc = "b(t)";
len = length(coef);
for i=len:-1:2
  myfunc =  strcat("b(t) * (", num2str(coef(i)), "+", myfunc, ")");
endfor
myfunc = strcat(num2str(coef(1)), "+", myfunc);

printf("b(t) = (t - 460) / 30\n");
printf("p_7(t) = %s\n", myfunc);

# Plotting interpolation
plot(x, y);
