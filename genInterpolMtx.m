function [A] = genInterpolMtx(base, x, y)
  n = length(x);
  A = zeros(n, n);
  for i=1:n
    for j=1:n
      A(i, j) = base(x(i), j - 1);
    endfor
  endfor
endfunction
