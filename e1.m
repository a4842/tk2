day = [184; 230; 276; 322; 368; 414; 460; 506];
covid = [3075; 4105; 8369; 11287; 6971; 6279; 6594; 38325];

base1 = @(t, i) t ^ i;
base2 = @(t, i) (t - 230) ^ i;
base3 = @(t, i) (t - 460) ^ i;
base4 = @(t, i) ((t - 460) / 30) ^ i;

A1 = genInterpolMtx(base1, day, covid)
A2 = genInterpolMtx(base2, day, covid)
A3 = genInterpolMtx(base3, day, covid)
A4 = genInterpolMtx(base4, day, covid)

conds = [cond(A1); cond(A2); cond(A3); cond(A4)];
bestIdx = 1;
for i=1:4
  printf("Conditional matrix base %d: %d\n",i, conds(i));  
  if conds(bestIdx) > conds(i)
    bestIdx = i;
  endif
endfor

printf("Best conditional matrix is base %d: %d\n",bestIdx, conds(bestIdx));