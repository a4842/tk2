function [coef, dd] = newtonInterpolCoef(x, y)
  % Calculate divided differences for the
  % Newton's interpolation coefficients
  n = length(x);
  coef = [y(1)];
  for i=1:n-1
    for j=1:n-i
      y(j) = (y(j+1) - y(j)) / (x(j+i) - x(j));
    endfor
    coef = [coef; y(1)];
  endfor 
  dd = y;
endfunction
