function [y_new] = newtonInterpolCalc(coef, x, x_new)
  function res = calculate(c, x, p)
    n = length(c);
    res = c(1);
    prod_accum = 1;
    for i=2:n
      prod_accum *= p - x(i-1);
      res += c(i) * prod_accum;
    endfor
  endfunction
  
  y_new = [];
  nxnew = length(x_new);
  for i=1:nxnew
    y_new = [y_new; calculate(coef, x, x_new(i))];
  endfor
endfunction
