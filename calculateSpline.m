function [y_new] = calculateSpline(x, mtx)
  n = length(x);
  display(n);
  y_new = [];
  
  function res = calculate(c)
    if (184 <= c < 230)
      res = mtx(1,1)*(c-184)^3 + mtx(1,2)*(c-184)^2 + mtx(1,3)*(c-184)+mtx(1,4);
    endif
    if (230 <= c < 276)
      res = mtx(2,1)*(c-184)^3 + mtx(2,2)*(c-184)^2 + mtx(2,3)*(c-184)+mtx(2,4);
    endif
    if (276 <= c < 322)
      res = mtx(3,1)*(c-184)^3 + mtx(3,2)*(c-184)^2 + mtx(3,3)*(c-184)+mtx(3,4);
    endif
    if (322 <= c < 368)
      res = mtx(4,1)*(c-184)^3 + mtx(4,2)*(c-184)^2 + mtx(4,3)*(c-184)+mtx(4,4);
    endif
    if (368 <= c < 414)
      res = mtx(5,1)*(c-184)^3 + mtx(5,2)*(c-184)^2 + mtx(5,3)*(c-184)+mtx(5,4);
    endif
    if (414 <= c < 460)
      res = mtx(6,1)*(c-184)^3 + mtx(6,2)*(c-184)^2 + mtx(6,3)*(c-184)+mtx(6,4);
    endif
    if (460 <= c < 506)
      res = mtx(7,1)*(c-184)^3 + mtx(7,2)*(c-184)^2 + mtx(7,3)*(c-184)+mtx(7,4);
    endif
  endfunction
  
  for i=1:n
    y_new = [y_new; calculate(x(i))];
  endfor
endfunction