function [y] = hornerInterpol(coef, base, x)
  function p = calc(coef, base, x)
    n = length(coef);
    p = 0;
    mul = base(x, 1);
    for i=n:-1:2
      p += coef(i);
      p *= mul;
    endfor
    p += coef(1);
  endfunction
  
  n = length(x);
  y = zeros(n, 1);
  for i=1:n
    y(i) = calc(coef, base, x(i));
  endfor
endfunction
