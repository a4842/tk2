day = [184; 230; 276; 322; 368; 414; 460; 506];
covid = [3075; 4105; 8369; 11287; 6971; 6279; 6594; 38325];

base = @(t, i) ((t - 460) / 30) ^ i;
A = genInterpolMtx(base, day, covid);
coef = A \ covid;

x = [552];
y = hornerInterpol(coef, base, x);

# Extrapolation untuk x > 506 pasti akan selalu menaik karena polinom
# berderajat 7 maksimal memiliki 6 titik balik, sedangkan hasil
# interpolasi sudah memiliki 6 titik balik di x < 506.
printf("Extrapolation on day 552: %d\n", y(1));
printf("Actual data: 5403\n");
printf("Difference: %d\n", y(1) - 5403);

# start = 184
# finish = 506
# x1 = linspace(start, finish, finish-start+1);
# y1 = hornerInterpol(coef, base, x1);
# plot(x1, y1);