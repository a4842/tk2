function [coef, dd] = newtonInterpolCoefExtra(coef, dd, x, y)
  nx = length(x);
  ncoef = length(coef);
  awal = ncoef;

  y(1:ncoef-1) = dd(1:ncoef-1);
  for i=1:nx-1
    for j=awal:nx-i
      y(j) = (y(j+1) - y(j)) / (x(j+i) - x(j));
    endfor
    if awal == 1
      coef = [coef; y(1)];
    endif
    awal = max(awal - 1, 1);
  endfor 
  dd = y;
endfunction
